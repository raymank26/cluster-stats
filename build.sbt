name := "akka-cluster"

version := "1.0"

scalaVersion := "2.11.7"

scalacOptions in Compile ++= Seq("-encoding", "UTF-8", "-target:jvm-1.8", "-deprecation",
    "-feature", "-unchecked", "-language:postfixOps", "-Xlog-reflective-calls", "-Xlint")

javacOptions in Compile ++= Seq("-source", "1.8", "-target", "1.8", "-Xlint:unchecked",
    "-Xlint:deprecation")

val akkaVersion = "2.4.0"
val akkaHttpVersion = "2.0-M1"

libraryDependencies ++= Seq(
    "com.typesafe.akka" %% "akka-actor" % akkaVersion,
    "com.typesafe.akka" %% "akka-remote" % akkaVersion,
    "com.typesafe.akka" %% "akka-cluster" % akkaVersion,
    "com.typesafe.akka" %% "akka-cluster-metrics" % akkaVersion,
    "com.typesafe.akka" %% "akka-cluster-tools" % akkaVersion,
    "com.typesafe.akka" %% "akka-multi-node-testkit" % akkaVersion,
    "com.typesafe.akka" %% "akka-http-core-experimental" % akkaHttpVersion,
    "com.typesafe.akka" %% "akka-http-experimental" % akkaHttpVersion,

    "ch.qos.logback" % "logback-classic" % "1.0.9",
    "org.kohsuke" % "wordnet-random-name" % "1.3",
    "org.json4s" %% "json4s-native" % "3.3.0"

)

javaOptions in run ++= Seq(
    "-Xms128m", "-Xmx1024m", "-Djava.library.path=./target/native")

Keys.fork in run := true

//mainClass in (Compile, run) := Some("sample.cluster.simple.SimpleClusterApp")
// make sure that MultiJvm test are compiled by the default test compilation
