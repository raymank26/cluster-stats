package com.raymank26.akka.cluster

import com.typesafe.config.ConfigFactory

/**
 * @author Anton Ermak.
 */
object ConfigurationProvider {

    def loadServerSettings(): HttpServer = {
        val appConfig = ConfigFactory.load().getConfig("app")
        HttpServer(appConfig.getString("host"), appConfig.getInt("port"))
    }

    case class HttpServer(host: String, port: Int)

}
