package com.raymank26.akka.cluster

import com.raymank26.akka.cluster.actors.MasterActor

import akka.actor.{ActorRef, Props}
import akka.http.scaladsl.Http
import akka.stream.ActorMaterializer

/**
 * Master startup hook. Master can be run from IDE or from sbt console by
 * `runMain com.raymank26.akka.cluster.StartMaster`
 *
 * @author Anton Ermak.
 */
object StartMaster extends Api {

    implicit lazy val system = ActorSystemFactory.getMasterActorSystem()

    override val masterRef: ActorRef = system.actorOf(Props[MasterActor])
    override implicit val ec = system.dispatcher

    implicit val materializer = ActorMaterializer()

    def main(args: Array[String]): Unit = {

        val serverSettings = ConfigurationProvider.loadServerSettings()
        val bindingFuture = Http().bindAndHandle(route, serverSettings.host, serverSettings.port)

        bindingFuture.onFailure {
            case _ => System.exit(1)
        }
    }
}
