package com.raymank26.akka.cluster

import com.typesafe.config.{Config, ConfigFactory}

import akka.actor.ActorSystem

/**
 * This factory provides [[ActorSystem]] for different cluster roles.
 *
 * For brevity we always start master on seed node, so if the master node goes to Down state we
 * should restart the Cluster.
 *
 * @author Anton Ermak.
 */
//noinspection AccessorLikeMethodIsEmptyParen
object ActorSystemFactory {

    def getMasterActorSystem(): ActorSystem = {
        val config = ConfigFactory.parseString("akka.cluster.roles = [master]")
            .withFallback(ConfigFactory.parseString("akka.remote.netty.tcp.port = 2551"))
            .withFallback(ConfigFactory.load())
        getSystem(config)
    }

    def getWorkerActorSystem(): ActorSystem = {
        val config = ConfigFactory.parseString("akka.cluster.roles = [worker]")
            .withFallback(ConfigFactory.parseString(s"akka.remote.netty.tcp.port = 0"))
            .withFallback(ConfigFactory.load())
        getSystem(config)
    }

    private def getSystem(config: Config) = {
        ActorSystem("ClusterSystem", config)
    }

}
