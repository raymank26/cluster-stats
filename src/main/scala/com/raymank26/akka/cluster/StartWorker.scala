package com.raymank26.akka.cluster

import com.raymank26.akka.cluster.actors.WorkerActor

import akka.actor.Props
import org.kohsuke.randname.RandomNameGenerator

/**
 * Worker startup hook. Master can be run from IDE or from sbt console by
 * `runMain com.raymank26.akka.cluster.StartWorker`
 *
 * @author Anton Ermak.
 */
object StartWorker {

    def main(args: Array[String]) {
        val rnd = new RandomNameGenerator()
        val system = ActorSystemFactory.getWorkerActorSystem()
        system.actorOf(Props[WorkerActor], name = rnd.next())
    }

}
