package com.raymank26.akka.cluster

import com.raymank26.akka.cluster.actors.MasterActor
import com.raymank26.akka.cluster.actors.MasterActor.{Exit, GetOverallInfo}
import com.raymank26.akka.cluster.actors.WorkerActor.ChangeInterval

import akka.actor.ActorRef
import akka.http.scaladsl.marshalling.{Marshaller, ToEntityMarshaller}
import akka.http.scaladsl.model.{ContentTypes, StatusCodes}
import akka.http.scaladsl.server.Directives._
import akka.pattern._
import akka.util.Timeout
import org.json4s._
import org.json4s.native.JsonMethods._

import scala.concurrent.ExecutionContext
import scala.concurrent.duration._

/**
 * REST API specification.
 *
 * @author Anton Ermak.
 */
trait Api {

    val masterRef: ActorRef

    implicit val jsonMarshaller: ToEntityMarshaller[JValue] = {
        Marshaller.StringMarshaller.wrap(ContentTypes.`application/json`)(json =>
            compact(render(json)))
    }

    implicit val ec: ExecutionContext
    implicit val timeout = Timeout(3 seconds)

    val route = pathPrefix("api" / "v1" / "workers") {
        get {
            pathEnd {
                complete {
                    (masterRef ? GetOverallInfo).mapTo[JValue]
                }
            } ~ path(Rest) { workerName =>
            complete {
                (masterRef ? MasterActor.GetNodeStatus(workerName)).mapTo[JValue]
            }
            }
        } ~ post {
            path("change-interval" / LongNumber) { time =>
                masterRef ! ChangeInterval(time)
                complete(StatusCodes.OK)
            } ~ pathEnd {
                StartWorker.main(Array())
                complete(StatusCodes.OK)
            }
        } ~ delete {
            path(Rest) { workerName =>
                masterRef ! Exit(workerName)
                complete(StatusCodes.OK)
            }
        }
    }
}
