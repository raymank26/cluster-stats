package com.raymank26.akka.cluster.actors

import com.raymank26.akka.cluster.actors.MasterActor.{Exit, GetNodeStatus, GetOverallInfo}
import com.raymank26.akka.cluster.actors.WorkerActor.{Stats, WorkerRegistration}

import akka.actor.{Actor, ActorLogging, ActorRef, PoisonPill, Terminated}
import akka.pattern._
import akka.util.Timeout
import org.json4s.JsonDSL._
import org.json4s._

import scala.concurrent.duration._
import scala.concurrent.{ExecutionContext, Future}

/**
 * Master actor. Aggregates information from [[WorkerActor]]s and provides interface for REST API.
 *
 * @author Anton Ermak.
 */
class MasterActor extends Actor with ActorLogging {

    implicit val timeout = Timeout(3 seconds)
    var workers = IndexedSeq.empty[ActorRef]

    import context.dispatcher

    @throws[Exception](classOf[Exception])
    override def preStart(): Unit = {
        context.system.scheduler.schedule(0 seconds, 2 seconds, new Runnable {
            override def run(): Unit = {
                log.info(workers.toString())
            }
        })
    }

    override def receive = {

        case WorkerRegistration if !workers.contains(sender()) =>
            log.info(s"New worker arrived ${sender() }")
            context.watch(sender())
            workers = workers :+ sender()

        case Terminated(worker) =>
            workers = workers.filterNot(_ == worker)

        case msg: WorkerActor.ChangeInterval =>
            workers.foreach { worker => worker ! msg }

        case Exit(name: String) =>
            workers.find(_.path.name == name) match {
                case Some(ref) => ref ! PoisonPill
                case None =>
            }

        case GetNodeStatus(name: String) =>
            workers.find(_.path.name == name) match {
                case Some(ref) =>
                    MasterActor.getWorkerStats(ref)
                        .map(MasterActor.serializeStats)
                        .pipeTo(sender())
                case None =>
                    log.info(s"No worker found with name = $name")
                    sender() ! ("error" -> "no such node with requested name found": JValue)
            }

        case GetOverallInfo =>
            val jobs: IndexedSeq[Future[JsonAST.JObject]] = workers.map { worker =>
                MasterActor.getWorkerStats(worker).map { value =>
                    MasterActor.serializeWorkerInfo(worker.path.name, value)
                }
            }
            Future.sequence(jobs).map { values =>
                JArray(values.toList)
            }.pipeTo(sender())
    }
}

object MasterActor {

    def serializeStats(stats: Stats): JObject = {
        ("overall-messages" -> stats.overall) ~
            ("messages-in-past-second" -> stats.messagesInLastSecond) ~
            ("interval" -> stats.askInterval)
    }

    def serializeWorkerInfo(name: String, stats: Stats): JObject = {
        ("name" -> name) ~
            ("stats" -> serializeStats(stats))
    }

    def getWorkerStats(ref: ActorRef)(implicit timeout: Timeout,
                                      ec: ExecutionContext): Future[Stats] = {
        (ref ? WorkerActor.AskThroughput).mapTo[Stats]
    }

    case class GetNodeStatus(name: String)

    case class Exit(name: String)

    case object GetOverallInfo

}
