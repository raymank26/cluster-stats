package com.raymank26.akka.cluster.actors

import com.raymank26.akka.cluster.actors.WorkerActor._

import akka.actor.{Actor, ActorLogging, ActorRef, Cancellable, RootActorPath, Terminated}
import akka.cluster.ClusterEvent.{CurrentClusterState, MemberUp}
import akka.cluster.{Cluster, Member, MemberStatus}

import scala.collection.immutable.HashMap
import scala.concurrent.duration._

/**
 * Worker peer. Actor handles incoming messages, sends them to another peer and collects self-usage
 * statistics.
 *
 * @author Anton Ermak.
 */
class WorkerActor extends Actor with ActorLogging {

    import context.dispatcher

    val cluster = Cluster(context.system)
    val system = context.system
    var workers = HashMap.empty[ActorRef, Cancellable]
    var overallMessages = 0
    var messagesInPastSecond = 0
    var messagesInCurrentSecond = 0

    var askInterval = 100 millis

    @throws[Exception](classOf[Exception])
    override def preStart(): Unit = {
        cluster.subscribe(self, classOf[MemberUp])
        cluster.registerOnMemberRemoved {
            system.terminate()
        }

        context.system.scheduler.schedule(0 seconds, 2 seconds, new Runnable {
            override def run(): Unit = {
                log.info(s"Adjacent workers = ${workers.toString() }")
            }
        })

        context.system.scheduler.schedule(0 seconds, 1 seconds, new Runnable {
            override def run(): Unit = self ! SwapMsgCounters
        })
    }

    @throws[Exception](classOf[Exception])
    override def postStop(): Unit = {
        log.info("Leaving cluster")
        cluster.leave(cluster.selfAddress)
        cluster.unsubscribe(self)
    }

    override def receive: Receive = {

        case MemberUp(member) =>
            log.info(s"New member up $member")
            register(member)

        case state: CurrentClusterState =>
            log.info(s"Current cluster state = ${state.toString }")
            state.members.filter(_.status == MemberStatus.Up).foreach(register)

        case Terminated(ref) =>
            workers(ref).cancel()
            workers = workers - ref

        case ChangeInterval(value) =>
            askInterval = FiniteDuration(value, "ms")
            workers = workers.map { case (ref, cancellable) =>
                cancellable.cancel()
                ref -> createTask(askInterval, ref)
            }

        case SwapMsgCounters =>
            messagesInPastSecond = messagesInCurrentSecond
            messagesInCurrentSecond = 0

        case WorkerRegistration if sender() != self && workers.get(sender()).isEmpty =>
            val task = createTask(askInterval, sender())
            workers = workers + (sender() -> task)

            context.watch(sender())

        case SimpleMessage =>
            log.info(s"Message received from ${sender() }")
            messagesInCurrentSecond += 1
            overallMessages += 1

        case AskThroughput =>
            log.info(s"Overall messages = $overallMessages")
            log.info(s"Messages in last second = $messagesInPastSecond")

            sender() ! Stats(overallMessages, messagesInPastSecond, askInterval.toMillis)
    }

    private def createTask(duration: FiniteDuration, ref: ActorRef) = {
        context.system.scheduler.schedule(0 milliseconds, duration, ref, SimpleMessage)
    }

    private def register(member: Member) =
        context.actorSelection(RootActorPath(member.address) / "user" / "*") !
            WorkerRegistration
}

object WorkerActor {

    case class Stats(overall: Long, messagesInLastSecond: Long, askInterval: Long)

    case class ChangeInterval(milliseconds: Long)

    case object WorkerRegistration

    case object SimpleMessage

    case object AskThroughput

    case object SwapMsgCounters

}

